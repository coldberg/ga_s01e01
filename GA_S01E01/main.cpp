#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <gl/glew.h>

#include <cassert>
#include <vector>
#include <iostream>

#include "gruft.hpp"
#include "shader.hpp"
#include "render.hpp"
#include "random.hpp"
#include "compare.hpp"
#include "drawing.hpp"
#include "mutator.hpp"

#include <algorithm>

int main (int argc, char** argv) 
{
	init ();
	auto& loc_render = Render::global ();
	auto& loc_config = Config::global ();
	auto& loc_random = Random::global ();
	auto& loc_compare = Compare::global ();
	auto& loc_mutate = Mutator::global ();

	auto loc_drawing = Drawing (loc_config, loc_random);
	

	auto* loc_master_sfc = SDL_LoadBMP("master.bmp");
	if (!loc_master_sfc)
	{
		std::cout << "master.bmp not found.\n";
		return -1;
	}

	loc_render.init ({loc_master_sfc->w, loc_master_sfc->h});
	loc_compare.init (*loc_master_sfc);

	std::uint32_t loc_last_result = 0xFFFFFFFFu;
	std::uint64_t loc_generation = 0;

	std::uint64_t loc_last_good_generation = 0;

    while (loop ()) 
    {
		auto loc_new_drawing = loc_drawing;

		loc_mutate.mutate_drawing (loc_random, loc_config, loc_new_drawing);
		loc_render.draw (loc_new_drawing);

		auto loc_result = loc_compare.difference (loc_render.texture ());

		++loc_generation;
/*
		if (loc_generation - loc_last_good_generation > loc_config.stalement_thresshold)
		{
			auto loc_stalement_index = loc_generation - loc_last_good_generation - loc_config.stalement_thresshold;

			loc_config.probability_of_append	   = glm::clamp (loc_config.probability_of_append       + loc_random.random_from_range(32768) / 32768.0f, 0.0f, 1.0f);
			loc_config.probability_of_remove	   = glm::clamp (loc_config.probability_of_remove       + loc_random.random_from_range(32768) / 32768.0f, 0.0f, 1.0f);
			loc_config.probability_of_reordering   = glm::clamp (loc_config.probability_of_reordering   + loc_random.random_from_range(32768) / 32768.0f, 0.0f, 1.0f);
			loc_config.probability_of_small_push   = glm::clamp (loc_config.probability_of_small_push   + loc_random.random_from_range(32768) / 32768.0f, 0.0f, 1.0f);
			loc_config.probability_of_medium_push  = glm::clamp (loc_config.probability_of_medium_push  + loc_random.random_from_range(32768) / 32768.0f, 0.0f, 1.0f);
			loc_config.probability_of_large_push   = glm::clamp (loc_config.probability_of_large_push   + loc_random.random_from_range(32768) / 32768.0f, 0.0f, 1.0f);
			loc_config.probability_of_color_change = glm::clamp (loc_config.probability_of_color_change + loc_random.random_from_range(32768) / 32768.0f, 0.0f, 1.0f);

			std::cout << "Mutated probability : "			
				<< "\n" << loc_config.probability_of_append
				<< "\n" << loc_config.probability_of_remove
				<< "\n" << loc_config.probability_of_reordering
				<< "\n" << loc_config.probability_of_small_push
				<< "\n" << loc_config.probability_of_medium_push
				<< "\n" << loc_config.probability_of_large_push
				<< "\n" << loc_config.probability_of_color_change
				<< "\n";
		}
*/
		if (loc_result < loc_last_result /*|| loc_random.flip_coin(loc_config.probability_of_bad_selection)*/)
		{
			loc_last_result = loc_result;
			auto loc_diff_gen = loc_generation - loc_last_good_generation;

			loc_last_good_generation = loc_generation;
			loc_drawing = loc_new_drawing;
			loc_render.preview (loc_compare);
			
			
			std::cout << " Difference : " << loc_result << "\n"
				<< " Primitives : " << loc_drawing.m_trigon.size() << "\n"
				<< " Generation : " << loc_generation << " (+" << loc_diff_gen << ") \n";

		}
		
		
    }

	loc_render.quit ();
    quit ();
	return 0;
}


