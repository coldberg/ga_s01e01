#pragma once

#include <cstdint>

#include <glm/glm.hpp>

#include <GL/glew.h>
#include <SDL.h>

struct Compare 
{
	Compare ();

	void init (const SDL_Surface& loc_surface);

	std::uint32_t difference (std::uint32_t loc_other_texture);

	std::uint32_t texture () const;
	std::uint32_t fbo () const;

	void quit ();

	static Compare& global ();

private:

	std::uint32_t m_target_texture = 0u;
	std::uint32_t m_result_buffer = 0u;
	glm::uvec2 m_target_extent = glm::uvec2(0);
	std::uint32_t m_fbo;
	

};
