#include "render.hpp"
#include "shader.hpp"

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <cassert>

void Render::init (const glm::uvec2& loc_size) 
{
	static const auto loc_black = glm::u8vec4 (0);

	glCreateTextures (GL_TEXTURE_2D, 1u, &m_render_texture);
	glTextureStorage2D (m_render_texture, 1u, GL_RGBA8, loc_size.x, loc_size.y);
	glClearTexSubImage (m_render_texture, 0u, 0u, 0u, 0u, loc_size.x, loc_size.y, 1u, GL_RGBA, GL_UNSIGNED_BYTE, &loc_black);

	glCreateFramebuffers (1u, &m_render_fbo);
	glNamedFramebufferTexture (m_render_fbo, GL_COLOR_ATTACHMENT0, m_render_texture, 0u);
	glNamedFramebufferDrawBuffer (m_render_fbo, GL_COLOR_ATTACHMENT0);
	glNamedFramebufferReadBuffer (m_render_fbo, GL_COLOR_ATTACHMENT0);

	assert (glCheckNamedFramebufferStatus (m_render_fbo, GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	glCreateVertexArrays (1u, &m_vertex_array);	
	glEnableVertexArrayAttrib (m_vertex_array, 0u);
	glVertexArrayAttribBinding (m_vertex_array, 0u, 0u);
	glVertexArrayAttribFormat (m_vertex_array, 0u, 2u, GL_SHORT, GL_TRUE, offsetof(vertex_type, locus));
	glEnableVertexArrayAttrib (m_vertex_array, 1u);
	glVertexArrayAttribBinding (m_vertex_array, 1u, 0u);
	glVertexArrayAttribFormat (m_vertex_array, 1u, 4u, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(vertex_type, color));

	m_render_size = loc_size;
}

void Render::quit () 
{
	if (m_render_fbo != 0u)
		glDeleteFramebuffers (1u, &m_render_fbo);
	if (m_render_texture != 0u)
		glDeleteTextures (1u, &m_render_texture);
	if (m_vertex_buffer != 0u)
		glDeleteBuffers (1u, &m_vertex_buffer);
	if (m_vertex_array != 0u)
		glDeleteVertexArrays (1u, &m_vertex_array);
}

Render::Render () 
{
}

Render& Render::global () {
	static Render _glob;
	return _glob;
}

void Render::draw (const Drawing& loc_drawing)
{
	static const auto loc_black = glm::vec4 (0.0f);
	static const auto loc_white = glm::vec4 (1.0f);
	static const auto loc_mat1 = glm::mat4(1.0f);

	const auto& loc_trigons = loc_drawing.m_trigon;
	const auto loc_total_size = loc_trigons.size () * sizeof (loc_trigons [0]);
	if (m_vertex_buffer_size < loc_total_size) 
	{
		if (m_vertex_buffer != 0u) 
		{
			glDeleteBuffers (1u, &m_vertex_buffer);
		}

		glCreateBuffers (1u, &m_vertex_buffer);
		glNamedBufferStorage (m_vertex_buffer, (GLsizei)loc_total_size, loc_trigons.data (),  GL_MAP_WRITE_BIT|GL_MAP_READ_BIT);
		glVertexArrayVertexBuffer (m_vertex_array, 0u, m_vertex_buffer, 0u, sizeof (vertex_type));

		m_vertex_buffer_size = loc_total_size;
	}
	else 
	{
		auto* loc_buff = glMapNamedBufferRange (m_vertex_buffer, 0u, loc_total_size, GL_MAP_WRITE_BIT);
		std::memcpy (loc_buff, loc_trigons.data (), loc_total_size);
		glUnmapNamedBuffer (m_vertex_buffer);
	}
	glm::vec4 loc_bgcolor = glm::vec4(loc_drawing.m_background) * (1.0f / 255.0f);
	glClearNamedFramebufferfv (m_render_fbo, GL_COLOR, 0, &loc_bgcolor.x);

	glDisable (GL_DEPTH_TEST);
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindFramebuffer (GL_FRAMEBUFFER, m_render_fbo);
	glViewport (0, 0, m_render_size.x, m_render_size.y);
	glBindVertexArray (m_vertex_array);

	auto loc_program = load_shader ("shaders/gradient");

	glUseProgram (loc_program);
	glProgramUniformMatrix4fv(loc_program, glGetUniformLocation(loc_program, "u_Transform"), 1u, GL_FALSE, &loc_mat1[0][0]);
	
	glDrawArrays (GL_TRIANGLES, 0u, GLsizei (loc_trigons.size () * 3u));
}

std::uint32_t Render::texture () const 
{
	return m_render_texture;
}

void Render::preview (const Compare& loc_compare) const 
{
	glBlitNamedFramebuffer (m_render_fbo, 0u, 0, 0, m_render_size.x, m_render_size.y, 
		0, 0, m_render_size.x, m_render_size.y, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	glBlitNamedFramebuffer (loc_compare.fbo(), 0u, 0, 0, m_render_size.x, m_render_size.y, 
		m_render_size.x+10, 0, m_render_size.x*2+10, m_render_size.y, GL_COLOR_BUFFER_BIT, GL_NEAREST);
}
