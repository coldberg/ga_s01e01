#include "compare.hpp"
#include "shader.hpp"

#include <memory>

#include <glm/glm.hpp>
#include <GL/glew.h>

const auto st_compare_cell_width = 32u;
const auto st_compare_cell_height = 32u;

Compare::Compare () {}

void Compare::init (const SDL_Surface& loc_surface) 
{
	if (m_target_texture != 0)
	{
		glDeleteTextures (1u, &m_target_texture);
	}

	if (m_fbo == 0)
	{
		glCreateFramebuffers (1u, &m_fbo);
	}

	auto loc_temp_data = std::make_unique<std::uint8_t[]>(loc_surface.pitch * loc_surface.h);
	for (auto i = 0; i < loc_surface.h; ++i)
	{
		std::memcpy(&loc_temp_data[i * loc_surface.pitch], &((const std::uint8_t*)loc_surface.pixels)[(loc_surface.h - i - 1)*loc_surface.pitch], loc_surface.pitch);
	}


	glCreateTextures (GL_TEXTURE_2D, 1u, &m_target_texture);
	glTextureStorage2D (m_target_texture, 1u, GL_RGBA8, loc_surface.w, loc_surface.h);
	glTextureSubImage2D (m_target_texture, 0u, 0u, 0u, loc_surface.w, loc_surface.h, GL_BGR, GL_UNSIGNED_BYTE, loc_temp_data.get ());

	glNamedFramebufferDrawBuffer (m_fbo, GL_COLOR_ATTACHMENT0);
	glNamedFramebufferReadBuffer (m_fbo, GL_COLOR_ATTACHMENT0);

	glNamedFramebufferTexture (m_fbo, GL_COLOR_ATTACHMENT0, m_target_texture, 0u);

	assert (glCheckNamedFramebufferStatus(m_fbo, GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	m_target_extent.x = loc_surface.w;
	m_target_extent.y = loc_surface.h;
}

std::uint32_t Compare::difference (std::uint32_t loc_other_texture) 
{
	static const std::uint32_t loc_zero = 0u;

	if (m_result_buffer == 0u)
	{
		glCreateBuffers (1u, &m_result_buffer);
		glNamedBufferStorage (m_result_buffer, sizeof (std::uint32_t), &loc_zero, GL_DYNAMIC_STORAGE_BIT|GL_MAP_WRITE_BIT|GL_MAP_READ_BIT);
	}
	else
	{
		glClearNamedBufferSubData(m_result_buffer, GL_R32UI, 0u, sizeof(std::uint32_t), GL_RED_INTEGER, GL_UNSIGNED_INT, &loc_zero);
	}

	auto loc_program = load_shader ("shaders/compare");
	glUseProgram (loc_program);

	glActiveTexture (GL_TEXTURE0);
	glBindTexture (GL_TEXTURE_2D, loc_other_texture);

	glActiveTexture (GL_TEXTURE1);
	glBindTexture (GL_TEXTURE_2D, m_target_texture);

	glProgramUniform1i (loc_program, glGetUniformLocation(loc_program, "u_TextureOne"), 0);
	glProgramUniform1i (loc_program, glGetUniformLocation(loc_program, "u_TextureTwo"), 1);

	glBindBufferBase (GL_SHADER_STORAGE_BUFFER, 2u, m_result_buffer);

	//glFenceSync (GL_SYNC_GPU_COMMANDS_COMPLETE, 0);	
	glDispatchCompute ((m_target_extent.x + st_compare_cell_width - 1)/st_compare_cell_width, 
					   (m_target_extent.y + st_compare_cell_height - 1)/st_compare_cell_height, 1u);	
	
	//glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	auto loc_result = *(const std::uint32_t*)glMapNamedBufferRange(m_result_buffer, 0u, sizeof(std::uint32_t), GL_MAP_READ_BIT);

	glUnmapNamedBuffer(m_result_buffer);

	return loc_result;
}

std::uint32_t Compare::texture () const {
	return m_target_texture;
}

std::uint32_t Compare::fbo () const {
	return m_fbo;
}

void Compare::quit () 
{
	if (m_result_buffer != 0u)
		glDeleteBuffers(1u, &m_result_buffer);

	if (m_target_texture != 0u)
		glDeleteTextures (1u, &m_target_texture);

	if (m_fbo != 0u)
		glDeleteFramebuffers (1u, &m_fbo);
}

Compare& Compare::global () {
	static Compare _glob;
	return _glob;
}
