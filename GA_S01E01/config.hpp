#pragma once

#include <cstddef>
#include <cstdint>

struct Config 
{
	float probability_of_append			= 1.0f / 16.0f;
	float probability_of_reordering		= 1.0f / 64.5f;
	float probability_of_remove			= 1.0f / 256.0f;
	

	float probability_of_small_push		= 1.0f / 128.0f;
	float probability_of_medium_push	= 1.0f / 4096.0f;
	float probability_of_large_push		= 1.0f / 32768.0f;
	float probability_of_color_change	= 1.0f / 16.0f;

	float probability_of_bad_selection  = 1.0f / 320.0f;

	int small_push_range				= 32768 / 1024;
	int medium_push_range				= 32768 / 32;
	int large_push_range				= 32768 / 1;
	int color_change_range				= 64;

	std::size_t trigon_maximum			= 10000;
	std::size_t trigon_minimum			= 1;

	std::uint64_t stalement_thresshold  = 200;

	static Config& global();
};
