#pragma once

#include <glm/glm.hpp>
#include <random>

#include "primitive.hpp"

struct Random
{
	glm::u16vec2 locus () const;
	glm::u8vec4 color () const;
	vertex_type vertex () const;
	trigon_type trigon () const;

	static Random& global ();
	
	bool flip_coin (float loc_probability) const;


	template <typename T>
	T random_from_range (T loc_min, T loc_max) const
	{
		std::uniform_int_distribution<T> loc_distr (loc_min, loc_max);
		return loc_distr (st_generator);
	}

	template <typename T>
	T random_from_range (T loc_range) const
	{
		return random_from_range (-loc_range, loc_range);
	}

private:
	static std::random_device st_device;
	static std::mt19937_64 st_generator;
};
