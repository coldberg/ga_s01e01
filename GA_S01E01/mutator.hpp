#pragma once

#include "drawing.hpp"
#include "random.hpp"
#include "config.hpp"

struct Mutator 
{	
	static Mutator& global ();

	void mutate_locus   (const Random& loc_random, const Config& loc_config, glm::u16vec2& loc_locus);
	void mutate_color   (const Random& loc_random, const Config& loc_config, glm::u8vec4& loc_color);
	void mutate_vertex  (const Random& loc_random, const Config& loc_config, vertex_type& loc_vertex);
	void mutate_trigon  (const Random& loc_random, const Config& loc_config, trigon_type& loc_trigon);

	void mutate_order   (const Random& loc_random, const Config& loc_config, std::vector<trigon_type>& loc_drawing);
	void mutate_append  (const Random& loc_random, const Config& loc_config, std::vector<trigon_type>& loc_drawing);
	void mutate_remove  (const Random& loc_random, const Config& loc_config, std::vector<trigon_type>& loc_drawing);
	
	void mutate_drawing (const Random& loc_random, const Config& loc_config, Drawing& loc_drawing);
};
