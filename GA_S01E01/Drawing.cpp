#include "drawing.hpp"
#include "random.hpp"
#include "config.hpp"

#include <GL/glew.h>


Drawing::Drawing (const Config& loc_config, const Random& loc_random)
:	m_trigon {},
	m_background (loc_random.color ())
{
	for (auto i = 0; i < loc_config.trigon_minimum; ++i)
	{
		m_trigon.push_back (loc_random.trigon ());
	}
}
