#pragma once

#include <glm/glm.hpp>

struct vertex_type
{
	glm::u16vec2 locus;
	glm::u8vec4 color; 
};

struct trigon_type 
{
	vertex_type vertex [3u]; 
};

