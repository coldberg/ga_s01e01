#pragma once

#include <string>
#include <list>
#include <cstdint>

auto map_enum (const std::uint64_t& loc_key) -> std::list<std::string>;
auto map_enum (const std::string& loc_key) -> std::list<std::uint64_t>;

