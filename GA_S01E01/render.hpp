#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "drawing.hpp"
#include "compare.hpp"

struct Render 
{
	void init (const glm::uvec2& loc_size);
	void quit ();
	
	Render ();
	
	Render (const Render&) = delete;
	Render& operator = (const Render&) = delete;

	static Render& global ();

	void draw (const Drawing& loc_drawing);	

	std::uint32_t texture () const;

	void preview(const Compare& loc_compare)const;

private:
	glm::uvec2		m_render_size			= glm::uvec2(0);
	std::size_t		m_vertex_buffer_size	= 0u;

	std::uint32_t	m_render_fbo			= 0u;
	std::uint32_t	m_render_texture		= 0u;
	std::uint32_t	m_vertex_buffer			= 0u;
	std::uint32_t	m_vertex_array			= 0u;
};
