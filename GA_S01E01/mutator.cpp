#include "mutator.hpp"

Mutator& Mutator::global () {
	static Mutator _glob;
	return _glob;
}

void Mutator::mutate_locus (const Random& loc_random, const Config& loc_config, glm::u16vec2& loc_locus) 
{
	auto loc_range = glm::ivec2 (0);
	if (loc_random.flip_coin (loc_config.probability_of_small_push))	
	{
		loc_range.x += loc_random.random_from_range (loc_config.small_push_range);
		loc_range.y += loc_random.random_from_range (loc_config.small_push_range);
	}
	if (loc_random.flip_coin (loc_config.probability_of_medium_push))	
	{
		loc_range.x += loc_random.random_from_range (loc_config.medium_push_range);
		loc_range.y += loc_random.random_from_range (loc_config.medium_push_range);
	}
	if (loc_random.flip_coin (loc_config.probability_of_large_push))	
	{
		loc_range.x += loc_random.random_from_range (loc_config.large_push_range);
		loc_range.y += loc_random.random_from_range (loc_config.large_push_range);
	}

	auto loc_result = glm::ivec2(loc_locus) + loc_range;
	loc_locus = glm::u16vec2 (glm::clamp (loc_result, glm::ivec2(0), glm::ivec2 (65535)));
}

void Mutator::mutate_color (const Random& loc_random, const Config& loc_config, glm::u8vec4& loc_color) 
{
	auto loc_range = glm::ivec4(0);
	if (loc_random.flip_coin (loc_config.probability_of_color_change))
	{
		loc_range.r = loc_random.random_from_range (loc_config.color_change_range);
		loc_range.g = loc_random.random_from_range (loc_config.color_change_range);
		loc_range.b = loc_random.random_from_range (loc_config.color_change_range);
		loc_range.a = loc_random.random_from_range (loc_config.color_change_range);
	}
	auto loc_result = glm::ivec4(loc_color) + loc_range;
	loc_color = glm::u8vec4 (glm::clamp (loc_result, glm::ivec4(0), glm::ivec4 (255)));
}

void Mutator::mutate_vertex (const Random& loc_random, const Config& loc_config, vertex_type& loc_vertex) 
{
	mutate_locus (loc_random, loc_config, loc_vertex.locus);
	mutate_color (loc_random, loc_config, loc_vertex.color);
}

void Mutator::mutate_trigon (const Random& loc_random, const Config& loc_config, trigon_type& loc_trigon) 
{
	mutate_vertex (loc_random, loc_config, loc_trigon.vertex [0]);
	mutate_vertex (loc_random, loc_config, loc_trigon.vertex [1]);
	mutate_vertex (loc_random, loc_config, loc_trigon.vertex [2]);
}

void Mutator::mutate_order (const Random& loc_random, const Config& loc_config, std::vector<trigon_type>& loc_drawing) 
{
	if (loc_random.flip_coin (loc_config.probability_of_reordering))
	{
		auto loc_index = loc_random.random_from_range<std::size_t> (0u, loc_drawing.size () - 1u);
		std::rotate (loc_drawing.rbegin () + loc_index, loc_drawing.rbegin () + loc_index + 1, loc_drawing.rend ());
	}
}

void Mutator::mutate_append (const Random& loc_random, const Config& loc_config, std::vector<trigon_type>& loc_drawing) 
{
	if (loc_drawing.size () < loc_config.trigon_maximum 
     && loc_random.flip_coin (loc_config.probability_of_append))
	{
		loc_drawing.push_back (loc_random.trigon ());
	}
}

void Mutator::mutate_remove (const Random& loc_random, const Config& loc_config, std::vector<trigon_type>& loc_drawing) 
{
	if (loc_drawing.size () > loc_config.trigon_minimum
     && loc_random.flip_coin (loc_config.probability_of_remove))
	{
		auto loc_index = loc_random.random_from_range<std::size_t> (0u, loc_drawing.size () - 1u);
		loc_drawing.erase (loc_drawing.begin () + loc_index);
	}
}

void Mutator::mutate_drawing (const Random& loc_random, const Config& loc_config, Drawing& loc_drawing) 
{
	mutate_append (loc_random, loc_config, loc_drawing.m_trigon);
	mutate_remove (loc_random, loc_config, loc_drawing.m_trigon);
	mutate_order (loc_random, loc_config,  loc_drawing.m_trigon);

	for (auto& loc_it : loc_drawing.m_trigon)
	{
		mutate_trigon (loc_random, loc_config, loc_it);
	}

	if (loc_random.flip_coin(loc_config.probability_of_color_change))
	{
		mutate_color(loc_random, loc_config, loc_drawing.m_background);
	}
}
