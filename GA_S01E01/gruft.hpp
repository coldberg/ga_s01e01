#pragma once

#include <glm/glm.hpp>

void init ();
void quit ();
bool loop ();

glm::ivec2 window_extent();
