#pragma once

#include <GL/glew.h>
#include <string>


GLint load_shader (const std::string& loc_path);
void preload_all_shaders (const std::string& loc_path);
void unload_all_shaders ();