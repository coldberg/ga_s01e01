#include "random.hpp"
#include <limits>
#include <cstdint>
#include <glm/gtc/constants.hpp>

glm::u16vec2 Random::locus () const 
{
	std::uniform_int_distribution<std::uint16_t> loc_distribute 
	(
		std::numeric_limits<std::uint16_t>::min (), 
		std::numeric_limits<std::uint16_t>::max ()
	);

	return glm::u16vec2
	(
		loc_distribute (st_generator), 
		loc_distribute (st_generator)
	);
}

glm::u8vec4 Random::color () const 
{
	std::uniform_int_distribution<std::uint16_t> loc_distribute 
	(
		std::numeric_limits<std::uint8_t>::min (), 
		std::numeric_limits<std::uint8_t>::max ()
	);

	return glm::u8vec4
	(
		loc_distribute (st_generator) & 0xff, 
		loc_distribute (st_generator) & 0xff,
		loc_distribute (st_generator) & 0xff,
		loc_distribute (st_generator) & 0xff
	);
}

vertex_type Random::vertex () const 
{
	return vertex_type 
	{
		locus (),
		color ()
	};
}

trigon_type Random::trigon () const 
{
	const auto _1o3sig = 2.0f/3.0f*glm::pi<float>();
	const auto _2o3sig = 4.0f/3.0f*glm::pi<float>();

	auto loc_center = locus();
	auto loc_radius = random_from_range (16, int(32768*0.125f));
	auto loc_colors = color();
	auto loc_rotate = random_from_range (32768)/32768.0f;

	return trigon_type 
	{
		vertex_type{glm::u16vec2 ((loc_center.x + std::sin (   0.0f + loc_rotate) * loc_radius), (loc_center.y + std::cos (   0.0f + loc_rotate) * loc_radius)), loc_colors},
		vertex_type{glm::u16vec2 ((loc_center.x + std::sin (_1o3sig + loc_rotate) * loc_radius), (loc_center.y + std::cos (_1o3sig + loc_rotate) * loc_radius)), loc_colors},
		vertex_type{glm::u16vec2 ((loc_center.x + std::sin (_2o3sig + loc_rotate) * loc_radius), (loc_center.y + std::cos (_2o3sig + loc_rotate) * loc_radius)), loc_colors}
	};

	//return trigon_type 
	//{
	//	vertex (),
	//	vertex (),
	//	vertex ()
	//};
}

Random& Random::global () 
{
	static Random _global;
	return _global;
}

bool Random::flip_coin (float loc_probability) const
{
	std::discrete_distribution<int> loc_distr ({1 - loc_probability, loc_probability});
	return loc_distr (st_generator) != 0;
}

std::random_device Random::st_device;
std::mt19937_64 Random::st_generator (Random::st_device ());