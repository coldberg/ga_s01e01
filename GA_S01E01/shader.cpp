#include "shader.hpp"

#include <cassert>
#include <iostream>
#include <algorithm>
#include <filesystem>
#include <unordered_map>
#include <memory>
#include <fstream>

static std::unordered_map<std::string, GLint> st_shader_cache;

GLint load_shader (const std::string& loc_path) {
	using namespace std::experimental::filesystem;

	const auto loc_key = path (loc_path).generic_string ();
	auto& loc_slot = st_shader_cache [loc_key];

	if (loc_slot != 0) {
		return loc_slot;
	}

	static const std::unordered_map<std::string, GLenum> loc_fext_to_type =
	{
		{".tesc", GL_TESS_CONTROL_SHADER},
		{".tese", GL_TESS_EVALUATION_SHADER},
		{".geom", GL_GEOMETRY_SHADER},
		{".vert", GL_VERTEX_SHADER},
		{".frag", GL_FRAGMENT_SHADER},
		{".comp", GL_COMPUTE_SHADER}
	};

	std::vector<GLint> loc_object_handle;
	auto loc_ival = 0;

	std::cout << "Building program `" << loc_path << "` ...\n";

	std::for_each (
		directory_iterator (loc_path),
		directory_iterator (),
		[&] (const auto& loc_file)
		{
			if (is_directory(loc_file.path ())) 
			{
				return;
			}

			auto loc_size = file_size (loc_file.path ());
			auto loc_text = std::make_unique<char []> (loc_size);

			std::ifstream loc_load_text (loc_file.path ());
			loc_load_text.read (loc_text.get (), loc_size);

			auto loc_fext = loc_file.path ().stem ().extension ().string ();
			auto loc_type = loc_fext_to_type.at (loc_fext);

			auto loc_sobj = glCreateShader (loc_type);

			auto loc_csrc = loc_text.get ();
			auto loc_leng = (GLsizei)loc_size;

			std::cout << "Compiling shader `" << loc_file.path().generic_string() << "` ...\n";

			glShaderSource (loc_sobj, 1u, &loc_csrc, &loc_leng);
			glCompileShader (loc_sobj);

			glGetShaderiv (loc_sobj, GL_INFO_LOG_LENGTH, &loc_ival);
			if (loc_ival > 0) 
			{
				auto loc_clog = std::make_unique<char []> (loc_ival + 1);
				glGetShaderInfoLog (loc_sobj, loc_ival + 1, &loc_ival, loc_clog.get ());
				std::cout << "Error while compiling `" << loc_file.path ().generic_string() << "`.\n";
				std::cout << loc_clog.get () << "\n";
				__debugbreak ();
			}

			glGetShaderiv (loc_sobj, GL_COMPILE_STATUS, &loc_ival);
			if (loc_ival != GL_TRUE) 
			{
				glDeleteShader (loc_sobj);
				std::cout << "Failed to compile shader `" << loc_file.path ().generic_string() << "`.\n\n";
				return;
			}

			loc_object_handle.push_back (loc_sobj);
		}
	);

	if (loc_object_handle.empty ()) {
		return 0;
	}

	GLint loc_handle = glCreateProgram ();

	for (const auto& it : loc_object_handle) {
		glAttachShader (loc_handle, it);
	}

	glLinkProgram (loc_handle);
	glGetProgramiv (loc_handle, GL_INFO_LOG_LENGTH, &loc_ival);

	if (loc_ival > 0) {
		auto loc_clog = std::make_unique<char []> (loc_ival + 1);
		glGetProgramInfoLog (loc_handle, loc_ival + 1, &loc_ival, loc_clog.get ());
		std::cout << "Error while linking `" << loc_path << "`.\n";
		std::cout << loc_clog.get () << "\n";
	}

	for (const auto& it : loc_object_handle) {
		glDetachShader (loc_handle, it);
		glDeleteShader (it);
	}

	glGetProgramiv (loc_handle, GL_LINK_STATUS, &loc_ival);
	if (loc_ival != GL_TRUE) {
		std::cout << "Failed to link program `" << loc_path << "`.\n";
		glDeleteProgram (loc_handle);
		return 0;
	}

	loc_slot = loc_handle;

	return loc_handle;
}

void preload_all_shaders (const std::string& loc_path) {
	using namespace std::experimental::filesystem;

	std::for_each(
		directory_iterator (loc_path),
		directory_iterator (),
		[] (const auto& loc_dir) 
		{
			if (is_directory (loc_dir.path ()))
			{
				load_shader (loc_dir.path ().generic_string());
			}
		}
	);
}

void unload_all_shaders () 
{
	for (auto& it : st_shader_cache) 
	{
		glDeleteProgram (it.second);
		it.second = 0;
	}
}
