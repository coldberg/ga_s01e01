#pragma once

#include "primitive.hpp"
#include "config.hpp"
#include "random.hpp"
#include <vector>

struct Drawing 
{	
	Drawing (const Config& loc_config, const Random& loc_random);

	std::vector<trigon_type> m_trigon;	
	glm::u8vec4 m_background;
};
