#include <GL/glew.h>
#include <SDL.h>

#include "shader.hpp"
#include "gruft.hpp"
#include "debug_strings.hpp"

#include <iostream>
#include <cassert>
#include <unordered_map>

static SDL_Window*		st_window = nullptr; 
static SDL_GLContext	st_opengl = nullptr; 


static const auto g_configuration = std::unordered_map<SDL_GLattr, int> {
	{SDL_GL_RED_SIZE,				8},
	{SDL_GL_GREEN_SIZE,				8},
	{SDL_GL_BLUE_SIZE,				8},
	{SDL_GL_ALPHA_SIZE,				8},

	{SDL_GL_STENCIL_SIZE,			8},
	{SDL_GL_DEPTH_SIZE,				24},

	{SDL_GL_MULTISAMPLEBUFFERS,		0},
	{SDL_GL_MULTISAMPLESAMPLES,		1},
	{SDL_GL_CONTEXT_MAJOR_VERSION,	4},
	{SDL_GL_CONTEXT_MINOR_VERSION,	5},
#if defined(_DEBUG)
	{SDL_GL_CONTEXT_FLAGS,			SDL_GL_CONTEXT_DEBUG_FLAG},
#endif
	{SDL_GL_CONTEXT_PROFILE_MASK,	SDL_GL_CONTEXT_PROFILE_CORE}
};

void init () 
{
    SDL_Init (SDL_INIT_EVERYTHING);

	for (const auto& it : g_configuration)
	{
		SDL_GL_SetAttribute (it.first, it.second);
	}

    st_window = SDL_CreateWindow (nullptr, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1920u, 1080u, SDL_WINDOW_OPENGL);
    st_opengl = SDL_GL_CreateContext (st_window);

	assert (st_window != nullptr);
	assert (st_opengl != nullptr);

	glewExperimental = true;
	auto loc_glewok = glewInit ();

	assert (loc_glewok == GLEW_OK);

#ifdef _DEBUG
	glEnable (GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback (
		[] (GLenum source,
		    GLenum type,
		    GLuint id,
		    GLenum severity,
		    GLsizei length,
		    const GLchar *message,
		    const void *userParam) 
		-> void
		{			
			if (severity != GL_DEBUG_SEVERITY_NOTIFICATION) 
			{
				std::cout << std::string (message, length) << "\n";
				std::cout << " >> " << map_enum (source).front () << "\n";
				std::cout << " >> " << map_enum (type).front () << "\n";
				std::cout << " >> " << map_enum (id).front () << "\n";
				std::cout << " >> " << map_enum (severity).front () << "\n";
				//__debugbreak ();
			}
		}, 
		nullptr
	);

	glDebugMessageControl (GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0u, nullptr, GL_TRUE);
#endif	
	
	preload_all_shaders ("shaders");
}

void quit ()
{
	unload_all_shaders ();

    SDL_GL_DeleteContext (st_opengl);
    SDL_DestroyWindow (st_window);

    SDL_Quit ();
}


bool loop ()
{
	SDL_Event loc_event;
	while (SDL_PollEvent (&loc_event)) 
	{
		if (loc_event.type == SDL_QUIT)
		{
			return false;
		}
	}

	SDL_GL_SwapWindow (st_window);
	return true;
}

glm::ivec2 window_extent () 
{
	glm::ivec2 loc_result = {0, 0};

	SDL_GetWindowSize (st_window, &loc_result.x, &loc_result.y);

	return loc_result;
}
