#version 450

#define RECT_HEIGHT	32
#define RECT_WIDTH	32

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

layout (std430, binding = 2) buffer sbo_Result
{
	uint u_Result;
};

layout(binding = 3) uniform sampler2D u_TextureOne;
layout(binding = 4) uniform sampler2D u_TextureTwo;

void main () { 
	const ivec2 loc_glob = ivec2 (gl_GlobalInvocationID.xy * ivec2 (RECT_WIDTH, RECT_HEIGHT));
	const ivec2 loc_size = textureSize(u_TextureOne, 0);

	uint loc_sum = 0;

	for (int j = 0; j < RECT_HEIGHT; ++j) {

		if (loc_glob.y + j >= loc_size.y)
			break;

		for (int i = 0; i < RECT_WIDTH; ++i) {

			if (loc_glob.x + i >= loc_size.x)
				break;

			const ivec2 loc_position = loc_glob + ivec2(i, j);

			ivec4 loc_texel0 = ivec4 (255.0f*texelFetch (u_TextureOne, loc_position, 0));
			ivec4 loc_texel1 = ivec4 (255.0f*texelFetch (u_TextureTwo, loc_position, 0));
			
			loc_sum += uint (abs(loc_texel0.r - loc_texel1.r));
			loc_sum += uint (abs(loc_texel0.g - loc_texel1.g));
			loc_sum += uint (abs(loc_texel0.b - loc_texel1.b));
			
		}
	}

	atomicAdd (u_Result, loc_sum);
}