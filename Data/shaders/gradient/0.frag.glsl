#version 450

uniform mat4 u_Transform;

in vec4 g2f_Position;
in vec4 g2f_Color;

layout(location = 0) out vec4 out_Color;

void main () 
{
	out_Color = g2f_Color;
}