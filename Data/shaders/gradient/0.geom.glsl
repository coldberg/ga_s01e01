#version 450

uniform int u_VerticesPerLayer;

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in gl_PerVertex
{
    vec4 gl_Position;
    float gl_PointSize;
    float gl_ClipDistance[];	
} gl_in[];

in int v2g_VertexID[];
in vec4 v2g_Position[];
in vec4 v2g_Color[];

out vec4 g2f_Position;
out vec4 g2f_Color;

void main () 
{	
	gl_Layer = 0; //int (v2g_VertexID[0] / u_VerticesPerLayer);

	gl_Position = gl_in[0].gl_Position;
	g2f_Color = v2g_Color[0];
	EmitVertex();

	gl_Position = gl_in[1].gl_Position;
	g2f_Color = v2g_Color[1];
	EmitVertex();

	gl_Position = gl_in[2].gl_Position;
	g2f_Color = v2g_Color[2];
	EmitVertex();
}