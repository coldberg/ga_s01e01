#version 450

uniform mat4 u_Transform;

layout(location = 0) in vec4 in_Position;
layout(location = 1) in vec4 in_Color;

out vec4 v2g_Position;
out vec4 v2g_Color;
out int v2g_VertexID;

void main () 
{
	v2g_VertexID = int(gl_VertexID);
	v2g_Position = gl_Position = u_Transform * in_Position;
	v2g_Color = in_Color;
}